<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>Jack F. Murphy</title>
    <link href="https://jack.engineering/feed.xml" rel="self" />
    <link href="https://jack.engineering" />
    <updated>2023-02-03T17:04:31+00:00</updated>
    <author>
        <name>Jack F. Murphy</name>
    </author>
    <id>https://jack.engineering</id>

    <entry>
        <title>Conference Proceedings</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/abstracts/"/>
        <id>https://jack.engineering/abstracts/</id>

        <updated>2022-11-07T13:56:43+00:00</updated>
            <summary>
                <![CDATA[
                    2019 J. F. Murphy et al., 'Cardiac Stem Cells from Failing Heart Improve Contractility of Engineered Human Myocardium', Biomedical Engineering&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                <h4>2019</h4>
<p><strong>J. F. Murphy </strong>et al., 'Cardiac Stem Cells from Failing Heart Improve Contractility of Engineered Human Myocardium', Biomedical Engineering Society Annual Meeting 2019.</p>
<p>S. I. Salazar et al., 'Comparison of cell culturing conditions and extracellular vesicles preservation techniques to maximize cardioactive potency of the adult stem cell secretome', New York Academy of the Sciences 2019.</p>
<h4>2018</h4>
<p><strong>J. F. Murphy</strong> et al., <a href="https://www.ahajournals.org/doi/abs/10.1161/circ.138.suppl_1.16448" target="_blank" rel="noopener noreferrer">'Adult Human Cardiac Progenitor Cells Enhance Contractile Force in Human Engineered Cardiac Tissues'</a>, AHA Scientific Sessions 2018.</p>
<p>I. C. Turnbull, <strong>J. F. Murphy</strong> et al., 'Heterocellular Coupling Mediates Pro-Contractile Effects Of Cardiac Progenitor Cells In Human Engineered Cardiac Tissue',</p>
<div>International Society for Stem Cell Research 2018.</div>
<div> </div>
<div><strong>J. F. Murphy</strong> et al., 'Cardiac Progenitor Cells Improve Function in Human Engineered Cardiac Tissues', New York City Science and Engineering Fair 2018.</div>
<div> </div>
            ]]>
        </content>
    </entry>
    <entry>
        <title>Education</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/jack-murphy/"/>
        <id>https://jack.engineering/jack-murphy/</id>

        <updated>2022-12-21T15:22:19+00:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/1/PXL_20221028_135517599.jpg" alt="" />
                    2022 - present The University of Cambridge PhD in Engineering, Biomedical - W.D Armstrong Studentship (2022-2026) - Honorary Robert Gardiner&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/1/PXL_20221028_135517599.jpg" alt="" />
                <p><strong>2022 - present</strong><br>The University of Cambridge<br><i>     </i> <em>PhD in Engineering, Biomedical</em><br>       - W.D Armstrong Studentship (2022-2026)<br><span dir="ltr" role="presentation">       - Honorary Robert Gardiner Memorial Scholarship (2022) </span><br><span dir="ltr" role="presentation">       - Engineering for Clinical Practice Grant (2022)</span></p>
<p><strong>2018 - 2022</strong><br>Trinity College Dublin<br><i>     Baccalaureus in Arte Ingeniaria</i> (B.A.I.),<em> Biomedical Engineering</em>       <br>       - First Class Honours<br><span dir="ltr" role="presentation">       - Thesis: ’Metabiomaterial: Multi-layed 3D immunomodulating patch’</span></p>
<p> </p>
            ]]>
        </content>
    </entry>
    <entry>
        <title>doi3bib</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/doi3bib/"/>
        <id>https://jack.engineering/doi3bib/</id>
            <category term="Projects"/>

        <updated>2022-10-16T16:15:15+01:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/4/doi3bib_screenshot-2.png" alt="" />
                    This project aims to create a robust platform that converts object identifiers into bibtex entries compatible with LaTeX. It is&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/4/doi3bib_screenshot-2.png" alt="" />
                <p>This project aims to create a robust platform that converts object identifiers into bibtex entries compatible with LaTeX. It is a simple javascript implementation that takes a DOI and converts it to a bibtex entry received from either doi.org or crossref.org. It parses that entry for known errors and corrects for them so they work correctly with LaTeX.</p>
<p>doi3bib is accessible at <a href="https://doi3bib.com">https://doi3bib.com</a>.</p>
            ]]>
        </content>
    </entry>
    <entry>
        <title>Rianu</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/rianu/"/>
        <id>https://jack.engineering/rianu/</id>
            <category term="Projects"/>

        <updated>2022-10-16T16:20:30+01:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/3/rianu_screenshot.png" alt="" />
                    Rianú is an open-source software, released under the BSD 3-Clause License, designed to more efficiently track and analyze engineered cardiac&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/3/rianu_screenshot.png" alt="" />
                <p>Rianú is an open-source software, released under the BSD 3-Clause License, designed to more efficiently track and analyze engineered cardiac tissues. </p>
<p>A manuscript pre-print is available at <a href="https://doi.org/10.2139/ssrn.4046466" target="_blank" rel="noopener noreferrer">https://doi.org/10.2139/ssrn.4046466</a>.</p>
<p>Documentation is available at <a href="https://rianu.mrph.dev" target="_blank" rel="noopener noreferrer">https://rianu.mrph.dev</a>.</p>
<p>Code is available at <a href="https://gitlab.com/hect-software/rianu" target="_blank" rel="noopener noreferrer">https://gitlab.com/hect-software/rianu</a>.</p>
            ]]>
        </content>
    </entry>
    <entry>
        <title>Publications</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/publications/"/>
        <id>https://jack.engineering/publications/</id>

        <updated>2022-11-07T13:54:15+00:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/2/IMG-4399.jpg" alt="" />
                    2022 J. F. Murphy, K. D. Costa, and I. C. Turnbull, ‘Rianú: Multi-Tissue Tracking Software for Increased Throughput of Engineered&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/2/IMG-4399.jpg" alt="" />
                <h4>2022</h4>
<p><strong>J. F. Murphy</strong>, K. D. Costa, and I. C. Turnbull, ‘Rianú: Multi-Tissue Tracking Software for Increased Throughput of Engineered Cardiac Tissue Screening’, SSRN Journal, 2022, doi: <a href="https://doi.org/10.2139/ssrn.4046466" target="_blank" rel="noopener noreferrer">10.2139/ssrn.4046466</a>. </p>
<h4>2019</h4>
<p><strong>J. F. Murphy </strong>et al., ‘Adult human cardiac stem cell supplementation effectively increases contractile function and maturation in human engineered cardiac tissues’, Stem Cell Res Ther, vol. 10, no. 1, p. 373, Dec. 2019, doi: <a href="https://doi.org/10.1186/s13287-019-1486-4" target="_blank" rel="noopener noreferrer">10.1186/s13287-019-1486-4</a>.</p>
<h4><strong>2018</strong></h4>
<p>I. C. Turnbull, J. Mayourian, <strong>J. F. Murphy</strong>, F. Stillitano, D. K. Ceholski, and K. D. Costa, ‘Cardiac Tissue Engineering Models of Inherited and Acquired Cardiomyopathies’, in Experimental Models of Cardiovascular Diseases, vol. 1816, K. Ishikawa, Ed. New York, NY: Springer New York, 2018, pp. 145–159. doi: <a href="https://doi.org/10.1007/978-1-4939-8597-5_11" target="_blank" rel="noopener noreferrer">10.1007/978-1-4939-8597-5_11</a>.</p>
<p>J. Mayourian et al., ‘Exosomal microRNA-21-5p Mediates Mesenchymal Stem Cell Paracrine Effects on Human Cardiac Tissue Contractility’, Circ Res, vol. 122, no. 7, pp. 933–944, Mar. 2018, doi: <a href="https://doi.org/10.1161/CIRCRESAHA.118.312420" target="_blank" rel="noopener noreferrer">10.1161/CIRCRESAHA.118.312420</a>.<br><br></p>
            ]]>
        </content>
    </entry>
    <entry>
        <title>Saor Crimp</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/saor-crimp/"/>
        <id>https://jack.engineering/saor-crimp/</id>
            <category term="Projects"/>

        <updated>2022-10-16T16:23:20+01:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/10/saor.jpg" alt="" />
                    This was a project carried out as part of the engineering coursework at Trinity College Dublin. We were tasked with&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/10/saor.jpg" alt="" />
                <p>This was a project carried out as part of the engineering coursework at Trinity College Dublin. We were tasked with designing a medical device using 3D printing technologies. We chose to design was a stent crimper that could be easily 3D printed and used on a lab bench or in the cath lab.</p>
<p>Project website is available at <a href="https://stent.mrph.dev" target="_blank" rel="noopener noreferrer">https://stent.mrph.dev</a>.</p>
            ]]>
        </content>
    </entry>
    <entry>
        <title>Outside of the Lab</title>
        <author>
            <name>Jack F. Murphy</name>
        </author>
        <link href="https://jack.engineering/outside-the-lab/"/>
        <id>https://jack.engineering/outside-the-lab/</id>

        <updated>2023-02-03T17:04:31+00:00</updated>
            <summary>
                <![CDATA[
                        <img src="https://jack.engineering/media/posts/7/20220731_173803.jpg" alt="" />
                    Hobby's Outside of research I enjoy staying active through a wide range of physical activities. Including rock climbing, hiking, obstacle&hellip;
                ]]>
            </summary>
        <content type="html">
            <![CDATA[
                    <img src="https://jack.engineering/media/posts/7/20220731_173803.jpg" alt="" />
                <h2>Hobby's</h2>
<p>Outside of research I enjoy staying active through a wide range of physical activities. Including rock climbing, hiking, obstacle course racing, and ice hockey. I recently joined the Cambridge Men's Blues Ice Hockey Team (<a href="https://cambridgeicehockey.com">CUIHC</a>). </p>
<div class="gallery-wrapper gallery-wrapper--full"><div class="gallery"  data-is-empty="false" data-translation="Add images" data-columns="6">
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/IMG_20220807_114532.jpg" data-size="4000x3000"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/IMG_20220807_114532-thumbnail.jpg" alt="" width="768" height="576"></a></figure>
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/13f0e371-9284-47b6-b5ad-f00a6705d71d.JPG" data-size="1600x1200"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/13f0e371-9284-47b6-b5ad-f00a6705d71d-thumbnail.JPG" alt="" width="768" height="576"></a></figure>
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/C843B570-5F30-4E33-8B1B-29B9A7DA31F0.JPG" data-size="1200x1200"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/C843B570-5F30-4E33-8B1B-29B9A7DA31F0-thumbnail.JPG" alt="" width="768" height="768"></a></figure>
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/gunks_back.jpg" data-size="2124x1416"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/gunks_back-thumbnail.jpg" alt="" width="768" height="512"></a></figure>
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/Ice-Hockey-398.jpg" data-size="7149x4766"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/Ice-Hockey-398-thumbnail.jpg" alt="" width="768" height="512"></a></figure>
<figure class="gallery__item"><a href="https://jack.engineering/media/posts/7/gallery/IMG-20230112-WA0008.jpg" data-size="2016x1512"><img loading="lazy" src="https://jack.engineering/media/posts/7/gallery/IMG-20230112-WA0008-thumbnail.jpg" alt="" width="768" height="576"></a></figure>
</div></div>
<p> </p>
            ]]>
        </content>
    </entry>
</feed>
